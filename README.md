# BeQATest
== Description

FahrApi project to demonstrate API testing with Java, Cucumber and Spring Framework

== Installation Instructions

Prerequisites:

* Java 11

* Maven > 3.3.9

Compile with Maven:

run from project root: `mvn clean compile`

Run tests:

run `mvn test`


**Get Location **
http://localhost:8081/getlocation?location=locationName

**Get Arrival **
http://localhost:8081/getarrival?locationId=id&date=2021-11-10
