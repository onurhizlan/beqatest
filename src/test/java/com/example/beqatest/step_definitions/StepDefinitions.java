package com.example.beqatest.step_definitions;

import com.example.beqatest.model.Arrival;
import com.example.beqatest.model.Location;
import com.example.beqatest.services.FahrServiceImpl;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import static org.junit.Assert.*;

public class StepDefinitions {
    private final FahrServiceImpl fahrService = new FahrServiceImpl();
    private List<Location> locations;
    private List<Arrival> arrivals;

    @Given("I call get location api {word}")
    public void callGetLocationApi(String location) {
        locations = fahrService.getLocations(location);
    }

    @Then("I should get a list of locations size$")
    public void shouldGetASizeOfLocation() {
        System.out.println(locations.size());
        assertFalse(locations.isEmpty());
    }

    @Given("I call get arrival api {word},{word}")
    public void callGetArrivalApi(String id, String date) throws IOException, ParseException {
    arrivals = fahrService.getArrivals(id, date);
    }

    @Then("I should get a list of arrivals size$")
    public void shouldGetASizeOfArrival() {
        System.out.println(arrivals.size());
        assertFalse(arrivals.isEmpty());
    }

    @Then("I should not get a list of size")
    public void shouldNotGetASizeOfLocation() {
        System.out.println(locations.size());
        assertNotEquals(locations.size(), 7);
    }
}
