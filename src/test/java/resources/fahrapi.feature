@fahrapi
Feature: FahrAPI

  Scenario: Get location
    Given I call get location api Berlin
    Then  I should get a list of locations size

  Scenario: Get arrival
    Given I call get arrival api 8096003,2021-07-01
    Then  I should get a list of arrivals size

  Scenario: Get wrong arrival size
    Given I call get location api Berlin
    Then  I should not get a list of size
