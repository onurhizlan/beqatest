package com.example.beqatest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@JsonIgnoreProperties(ignoreUnknown = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
public class Arrival {
  String name;
  String type;
  Integer boardId;
  Integer stopId;
  String stopName;
  String dateTime;
  String origin;
  String track;
  String detailsId;
}
