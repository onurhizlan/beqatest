package com.example.beqatest.controller;

import com.example.beqatest.model.Arrival;
import com.example.beqatest.model.Location;
import com.example.beqatest.services.FahrServiceImpl;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

@RestController
public class FarhController {

  private final FahrServiceImpl fahrService;

  public FarhController(FahrServiceImpl fahrService) {
    this.fahrService = fahrService;
  }

  @GetMapping("/getlocation")
  public ResponseEntity<List<Location>> getLocation(
      @RequestParam(name = "location") String location) {
    List<Location> locations = fahrService.getLocations(location);
    if (!locations.isEmpty()) return new ResponseEntity<>(locations, HttpStatus.OK);
    else return new ResponseEntity<>(locations, HttpStatus.NO_CONTENT);
  }

  @GetMapping("/getarrival")
  public ResponseEntity<List<Arrival>> getArrival(
      @RequestParam(name = "locationId") String locationId,
      @RequestParam(name = "date") @DateTimeFormat(pattern = "yyyy-MM-dd") String date) {
    try {
      List<Arrival> locations = fahrService.getArrivals(locationId, date);
      if (!locations.isEmpty()) return new ResponseEntity<>(locations, HttpStatus.OK);
      else return new ResponseEntity<>(locations, HttpStatus.NO_CONTENT);

    } catch (IOException | ParseException e) {
      e.printStackTrace();
    }
    return ResponseEntity.badRequest().build();
  }
}
