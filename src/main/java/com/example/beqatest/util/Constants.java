package com.example.beqatest.util;

public class Constants {
  public static final String GET_LOCATION_URL =
      "https://api.deutschebahn.com/freeplan/v1/location/%s";
  public static final String GET_ARRIVAL_URL =
      "https://api.deutschebahn.com/freeplan/v1/arrivalBoard/%s?date=%s";
}
