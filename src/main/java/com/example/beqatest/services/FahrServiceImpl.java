package com.example.beqatest.services;

import com.example.beqatest.config.ArrivalDao;
import com.example.beqatest.config.LocationDao;
import com.example.beqatest.model.Arrival;
import com.example.beqatest.model.Location;
import com.example.beqatest.util.Constants;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE)
@Service
public class FahrServiceImpl implements FahrService {

  final HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();
  final LocationDao locationDao = new LocationDao();
  final ArrivalDao arrivalDao = new ArrivalDao();

  @Override
  public List<Location> getLocations(String locationName) {
    String url = String.format(Constants.GET_LOCATION_URL, locationName);
    String jsonResponse = sendGet(url);
    try {
      return jsonResponse != null ? locationDao.load(jsonResponse) : null;
    } catch (IOException e) {
      e.printStackTrace();
    }
    return Collections.emptyList();
  }

  @Override
  public List<Arrival> getArrivals(String locationId, String dateString)
      throws IOException, ParseException {
    DateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
    String convertedCurrentDate = dt.format(dt.parse(dateString));
    String url = String.format(Constants.GET_ARRIVAL_URL, locationId, convertedCurrentDate);
    String jsonResponse = sendGet(url);
    return jsonResponse != null ? arrivalDao.load(jsonResponse) : null;
  }

  private String sendGet(String url) {
    try {
      HttpRequest request =
          HttpRequest.newBuilder()
              .GET()
              .uri(URI.create(url))
              .build();

      HttpResponse<String> response =
          httpClient.send(request, HttpResponse.BodyHandlers.ofString());

      if (response.statusCode() == 200) return response.body();
    } catch (Exception e) {
      log.error(e.getMessage());
      Thread.currentThread().interrupt();
    }
    return null;
  }
}
