package com.example.beqatest.services;

import com.example.beqatest.model.Arrival;
import com.example.beqatest.model.Location;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public interface FahrService {

  List<Location> getLocations(String locationName);

  List<Arrival> getArrivals(String locationId, String date) throws IOException, ParseException;
}
