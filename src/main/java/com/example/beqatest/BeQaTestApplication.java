package com.example.beqatest;

import com.example.beqatest.config.LocationDao;
import com.example.beqatest.model.Location;
import com.example.beqatest.services.FahrServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SpringBootApplication
public class BeQaTestApplication {
  public static void main(String[] args) throws IOException, ParseException {
    SpringApplication.run(BeQaTestApplication.class, args);
  }
}
