package com.example.beqatest.config;

import com.example.beqatest.model.Arrival;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

public class ArrivalDao {

  private final ObjectMapper mapper = new ObjectMapper();

  public List<Arrival> load(String jsonLocation) throws IOException {
    return mapper.readValue(jsonLocation, new TypeReference<>() {});
  }
}
