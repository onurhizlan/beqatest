package com.example.beqatest.config;

import com.example.beqatest.model.Location;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

public class LocationDao {

  private final ObjectMapper mapper = new ObjectMapper();

  public List<Location> load(String jsonLocation) throws IOException {
    return mapper.readValue(jsonLocation, new TypeReference<>() {});
  }
}
